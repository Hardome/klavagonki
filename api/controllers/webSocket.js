const config = require('../knexfile');
const knex = require('knex')(config.development);
module.exports = (server) => {
  const {WebSocketServer} = require('ws');
  const wss = new WebSocketServer({server});

  const {
    connect,
    invite,
    inviteResult,
    makeMove,
    gameEnd,
    getChat,
    sendMessage,
    closeConnection
  } = require('../services/webSocket')

  let activePlayers = [];

  wss.on('connection', (ws, request) => {
    ws.on('message', async (message) => {

      const mes = JSON.parse(message);
      console.log(mes)
      switch (mes.method) {
        case 'connect':
          activePlayers = connect(ws, wss, mes, activePlayers);
          break;

        case 'invite':
          invite(ws, wss, mes);
          break;

        case 'inviteResult':
          await inviteResult(ws, wss, mes, knex);
          break;

        case 'makeMove':
          await makeMove(ws, wss, mes, knex);
          break;

        case 'gameEnd':
          await gameEnd(ws, wss, mes, knex);
          break;

        case 'getChat':
          await getChat(ws, wss, mes, knex);
          break;

        case 'sendMessage':
          await sendMessage(ws, wss, mes, knex);
          break;
      }

      ws.on('close', (message) => {
        activePlayers = closeConnection(ws, wss, mes, activePlayers);
      });
    })
  });
}