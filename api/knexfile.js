// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'pg',
    connection: {
      //host: 'postgres', //без докера закомментить
      database: 'klavagonki',
      user: 'postgres',
      password: 'root',
      port: 5432
    },
  },
};
