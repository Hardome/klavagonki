const express = require('express');
const cors = require('cors');
const authRouter = require('./routes/auth');
const app = express();
const {createServer} = require('http');
const server = createServer(app);
const webSocket = require('./controllers/webSocket');

webSocket(server);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/auth', authRouter);

server.listen(8000);

module.exports = app;