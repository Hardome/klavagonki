/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.createTable('users', (table) => {
    table.increments('id');
    table.string('surName').notNullable();
    table.string('firstName').notNullable();
    table.string('secondName').notNullable();
    table.integer('age').notNullable();
    table.boolean('gender').notNullable();
    table.boolean('statusActive').notNullable();
    table.boolean('statusGame').notNullable();
    table.timestamp('createdAt').notNullable();
    table.timestamp('updatedAt').notNullable();
  });

  await knex.schema.createTable('accounts', (table)=>{
    table.integer('idUser');
    table.foreign('idUser')
      .references('id')
      .inTable('users');
    table.string('login').notNullable();
    table.string('password').notNullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.dropTable('users');
  await knex.schema.dropTable('accounts');
};
