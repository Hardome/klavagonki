const {login} = require('./login');
const {check} = require('./check');

module.exports = {
  login,
  check
};