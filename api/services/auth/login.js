const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = async (knex, req) => {
  const {login, password} = req.body;

  const [user] = await knex('accounts')
    .where('login', login)
    .select('idUser', 'password');

  if(!user){
    throw Error('Пользователь с таким логином не найден');
  }

  const validPassword = bcrypt.compareSync(password, user.password);

  if(!validPassword){
    throw Error('Пароль неверный');
  }

  const [userData] = await knex('users')
    .where('id', user.idUser)
    .select('firstName', 'secondName', 'surName', 'id');

  return {token: createToken(user.idUser, userData), userData}
}

const createToken = (id, data) => {
  let payload = {
    id,
    firstName: data.firstName,
    secondName: data.secondName,
    surName: data.surName
  }
  return jwt.sign(payload, 'SecretKey');
}

module.exports = {
  login
};