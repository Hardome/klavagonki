const {connect} = require('./connect');
/*const {invite} = require('./invite');
const {inviteResult} = require('./inviteResult');
const {makeMove} = require('./makeMove');
const {gameEnd} = require('./gameEnd');
const {getChat} = require('./getChat');
const {sendMessage} = require('./sendMessage');*/
const {closeConnection} = require('./closeConnection');

module.exports = {
  connect,
/*  invite,
  inviteResult,
  makeMove,
  gameEnd,
  getChat,
  sendMessage,*/
  closeConnection
};