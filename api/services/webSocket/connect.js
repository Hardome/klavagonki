const connect = (ws, wss, mes, activePlayers) => {
  ws.id = mes.userId;
  if (!activePlayers.includes(mes.userId)) {
    activePlayers.push(mes.userId);
  }

  wss.clients.forEach(client => {
    client.send(JSON.stringify({method: 'connect', activePlayers}));
  })

  return activePlayers;
}

module.exports = {
  connect
};