const closeConnection = (ws, wss, mes, activePlayers) => {
  let activePlayersOnDisconnect = [];

  wss.clients.forEach(client => activePlayersOnDisconnect.push(client.id));
  const arrayEqual = JSON.stringify(activePlayersOnDisconnect) === JSON.stringify(activePlayers);

  if (!arrayEqual) {
    activePlayers = activePlayersOnDisconnect;
    wss.clients.forEach(client => {
      client.send(JSON.stringify({method: 'disconnect', activePlayers}));
    })
    console.log(activePlayers)
  }

  return activePlayers;
}

module.exports = {
  closeConnection
};