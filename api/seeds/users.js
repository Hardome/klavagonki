const {hashSync} = require("bcrypt");
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('accounts').del();
  await knex('users').del();

  const startUsers = [
    {
      surName: 'Бедрин',
      firstName: 'Семён',
      secondName: 'Олегович',
      age: 20,
      gender: true,
      statusActive: true,
      statusGame: false,
      createdAt: new Date(Date.now()),
      updatedAt: '2023-05-08 15:32'
    },
    {
      surName: 'Ситникова',
      firstName: 'Яна',
      secondName: 'Эдуардовна',
      age: 29,
      gender: false,
      statusActive: true,
      statusGame: false,
      createdAt: new Date(Date.now()),
      updatedAt: '2023-05-08 15:32'
    },
    {
      surName: 'Пушкин',
      firstName: 'Павел',
      secondName: 'Петрович',
      age: 67,
      gender: true,
      statusActive: true,
      statusGame: false,
      createdAt: new Date(Date.now()),
      updatedAt: '2023-05-08 15:32'
    },
  ];

  let _users = await knex('users').insert(startUsers).returning('*');
  console.log('\nInserted users:', _users);

  let _accounts = await knex('accounts').insert([
    {
      idUser: _users[0].id,
      login: 'Bedrin',
      password: hashSync('123456', 7)
    },
    {
      idUser: _users[1].id,
      login: 'Sitnokova',
      password: hashSync('Moloko', 7)
    },
    {
      idUser: _users[2].id,
      login: 'Pushkin',
      password: hashSync('Napisal', 7)
    }
  ]).returning('*');
  console.log('\nInserted accounts:', _accounts);
};
