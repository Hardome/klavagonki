import {makeAutoObservable} from 'mobx';

class UserStore {
  constructor() {
    makeAutoObservable(this, null, {autoBind: true});
  }

  loginValue = '';
  passwordValue = '';
  incorrectLogin = false;
  incorrectPassword = false;
  userData = {};
  isAuthorized = false;

  changeLogin = (value) => {
    this.loginValue = value;
  }

  changePassword = (value) => {
    this.passwordValue = value;
  }

  setIncorrectLogin = (value) =>{
    this.incorrectLogin = value;
  }

  setIncorrectPassword = (value) =>{
    this.incorrectPassword = value;
  }

  setUserData = (userData) =>{
    this.userData = userData;
  }

  setAuthorized = () => {
    this.isAuthorized = true;
    this.setIncorrectPassword(false);
    this.setIncorrectLogin(false);
  }

  setLogout = () => {
    this.isAuthorized = false;
    localStorage.removeItem('token');
  }

  auth = async () => {
    const res = await fetch('http://localhost:8000/auth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        login: this.loginValue,
        password: this.passwordValue
      })
    });

    const {token, userData, error} = await res.json();

    if (res.ok) {
      this.setAuthorized();
      this.setUserData(userData);
      localStorage.setItem('token', token);
    } else {
      if(error === 'Пароль неверный') {
        this.setIncorrectPassword(true);
      } else if (error === 'Пользователь с таким логином не найден') {
        this.setIncorrectLogin(true);
      }
    }
  }

  init = async () => {
    const token = localStorage.getItem('token');
    try {
      const res = await fetch('http://localhost:8000/auth/check', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({token})
      });
      const userData = await res.json();
      this.setUserData(userData);
      this.setAuthorized();
    } catch (e) {
      localStorage.removeItem(token);
      if (window.location.pathname === '/') {
        return;
      }
      window.location.pathname = '/';
    }
  }
}

export default new UserStore();