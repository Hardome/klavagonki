import {makeAutoObservable} from 'mobx';

class UserStore {
  constructor() {
    makeAutoObservable(this, null, {autoBind: true});
  }

  text = 'Пока, пока какая.';
  wordsArray = this.text.split(' ');
  enteredText = '';
  inputText = '';
  incorrectText = '';
  lastValue = '';

  changeInputText = (value) => {
    this.inputText = value;

    if(this.lastValue.length > value.length){
      if(this.incorrectText.length){
        this.text = this.incorrectText[this.incorrectText.length-1] + this.text;
        this.incorrectText = this.incorrectText.substring(0, this.incorrectText.length-1);
      } else {
        this.text = this.enteredText[this.enteredText.length-1] + this.text;
        this.enteredText = this.enteredText.substring(0, this.enteredText.length-1);
      }
    } else {
      const nextChar = this.text[0];
      if(nextChar === value[value.length-1] && this.incorrectText.length === 0){
        console.log('Правильно')
        this.enteredText += nextChar;
        if(!this.incorrectText.length){
          this.text = this.text.replace(nextChar, '');
        }
      } else {
        if(!this.text.length){
          return;
        }
        const nextIncorrectChar = this.text[0];
        this.incorrectText += nextIncorrectChar;
        this.text = this.text.replace(this.text[0], '');
      }
    }
    this.lastValue = value;

    const nextWord = this.wordsArray[0];
    if(value === nextWord + ' ') {
      this.wordsArray.shift();
      this.inputText = '';
      this.lastValue = '';
    } else if(this.wordsArray.length === 1 && nextWord === value){
      this.wordsArray.shift();
      this.inputText = '';
      this.lastValue = '';
    }
  }
}

export default new UserStore();