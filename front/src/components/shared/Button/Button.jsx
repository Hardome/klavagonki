import React from 'react';
import s from './Button.module.scss'
import {observer} from "mobx-react";

const Button = (props) => {

  return (
    <button
      className={s.button}
      onClick={props.onClick}>{props.text}
    </button>
  )
};

export default observer(Button);