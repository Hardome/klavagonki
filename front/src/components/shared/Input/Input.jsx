import React from 'react';
import s from './Input.module.scss'
import {observer} from "mobx-react";
import cn from 'classnames';

const Input = (props) => {
  const {variant} = props;
  let variantClassName;

  switch (variant) {
    case 'base':
      variantClassName = s.base;
      break;
    default:
      variantClassName = s.base;
  }

  return (
    <input
      className={cn(s.input, variantClassName)}
      onChange={props.onChange}
      value={props.value}
      type={props.type}
      placeholder={props.text}>
    </input>
  )
};

export default observer(Input);