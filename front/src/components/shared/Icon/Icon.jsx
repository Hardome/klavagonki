import React from 'react';
import s from './Icon.module.scss'

const Icon = () => {

  return (
    <div className={s.icon}></div>
  )
};

export default Icon;