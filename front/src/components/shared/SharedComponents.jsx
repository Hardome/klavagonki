import React from 'react';
import s from './SharedComponents.module.scss'
import Input from "./Input/Input";

const Shared = () => {

  return (
    <div className={s.page}>
      <Input />
    </div>
  )
};

export default Shared;