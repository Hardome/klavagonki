import React from "react";
import s from './ActivePlayers.module.scss';
import Text from '../../components/shared/Text/Text';

const ActivePlayer = (props) => {

  return (
      <div className={s.tableContent}>
        <div className={s.name}>
          <Text text={'Фамиллия'} />
          <Text text={'Имя'} />
          <Text text={'Отчество'} />
        </div>
        <Text text={'Статус в игре или нет'} />
        <Text text={'Пригласить или занят'} />
      </div>
  )
};

export default (ActivePlayer);