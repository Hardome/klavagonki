import React from "react";
import s from './ActivePlayers.module.scss'
import ActivePlayer from "./ActivePlayer";

const ActivePlayers = () => {

  return (
      <div className={s.content}>
        <div className={s.table}>
          <ActivePlayer />
        </div>
      </div>
  )
};

export default (ActivePlayers);