import React from 'react';
import s from './Game.module.scss'
import {inject, observer} from "mobx-react";
import Input from "../shared/Input/Input";

const Game = (props) => {
  const store = props.stores.gameStore;
  const {
    text,
    inputText,
    changeInputText,
    enteredText,
    incorrectText
  } = store;

  return (
    <div className={s.container}>
      <div className={s.gameText}>
        <pre className={s.enteredText}>{enteredText}</pre>
        <pre className={s.incorrectText}>{incorrectText}</pre>
        <pre className={s.text}>{text}</pre>
      </div>
      <Input
        onChange={(e) => changeInputText(e.target.value)}
        value={inputText}
      />
    </div>
  )
};

export default inject('stores')(observer(Game));