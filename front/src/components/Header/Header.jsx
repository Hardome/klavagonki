import React, {useEffect} from 'react';
import s from './Header.module.scss'
import {NavLink, useNavigate} from "react-router-dom";
import {inject, observer} from "mobx-react";
import Text from '../shared/Text/Text';
import Icon from "../shared/Icon/Icon";
import Socket from '../../webSocket';

const Header = (props) => {
  const store = props.stores.userStore;
  const withLinks = props.withLinks === undefined;
  const navigate = useNavigate();
  const logout = () => {
    store.setLogout();
    navigate('/');
    Socket.logout(store.userData.id);
  };

  useEffect(() => {
    if(window.location.pathname === '/'){
      return;
    }
    store.init().then(()=>{
      const userData = store.userData;
      if(userData.id){
        Socket.setSocket();
        Socket.ws.onopen = () => {
          Socket.connect(store.userData.id);
        }
      }
    });
  }, [store.isAuthorized]);

  return (
    <div className={s.header}>
      <div className={s.homeLink}>
        <div className={s.icon}>
          <Icon />
        </div>
        <Text variant={'font24'} text={'Клавагонки'} color={'White'} />
      </div>
      <div className={s.links}>
        { withLinks && (<NavLink className={s.link} to={'/game'}>Играть</NavLink>) }
        { withLinks && (<NavLink className={s.link} to={'/rating'}>Рейтинг</NavLink>) }
        { withLinks && (<NavLink className={s.link} to={'/active-players'}>Активные игроки</NavLink>) }
        { withLinks && (<NavLink className={s.link} to={'/games'}>История игр</NavLink>) }
        { withLinks && (<NavLink className={s.link} to={'/players'}>Список игроков</NavLink>) }
      </div>
      { withLinks && (
        <div className={s.exitButton}
             onClick={logout}
        >Выйти
        </div>
      ) }
    </div>
  )
};

export default inject('stores')(observer(Header));