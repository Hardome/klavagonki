import React from 'react';
import s from './Auth.module.scss'
import Input from '../shared/Input/Input';
import {inject, observer} from 'mobx-react';
import Button from '../shared/Button/Button';
import Icon from '../shared/Icon/Icon';
import {useNavigate} from 'react-router-dom';
import Text from "../shared/Text/Text";

const Authorization = (props) => {
  const store = props.stores.userStore;
  const navigate = useNavigate();
  const {
    loginValue,
    incorrectLogin,
    changeLogin,
    passwordValue,
    incorrectPassword,
    changePassword,
    auth
  } = store;

  const authorization = async () => {
    await auth();
    const isAuthorized = store.isAuthorized;
    if (isAuthorized) {
      navigate('/active-players');
    }
  }

  return (
    <div className={s.body}>
      <div className={s.form}>
        <div className={s.icon}>
          <Icon />
        </div>
        <Text
          className={s.text}
          variant={'font24'}
          text={'Авторизация'}
          color={'Black'}
        />
        <div className={s.input}>
          <Input
            text={'Логин'}
            onChange={(e) => changeLogin(e.target.value)}
            value={loginValue}
            isCorrect={incorrectLogin}
          />
          <span
            className={s.span}>{incorrectLogin ? 'Неверный логин' : ''}
          </span>
        </div>
        <div className={s.input}>
          <Input
            text={'Пароль'}
            onChange={(e) => changePassword(e.target.value)}
            value={passwordValue}
            isCorrect={incorrectPassword}
            type={'password'}
          />
          <span
            className={s.span}>{incorrectPassword ? 'Неверный пароль' : ''}
          </span>
        </div>
        <Button
          onClick={authorization}
          text={'Вход'}
        />
      </div>
    </div>
  )
};

export default inject('stores')(observer(Authorization));