import React from 'react';
import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom';
import Auth from './components/Authorization/Auth'
import WithHeader from "./components/WithHeader/WithHeader";
import ActivePlayers from "./components/ActivePlayers/ActivePlayers";
import Game from "./components/Game/Game";


const router = createBrowserRouter([
  {
    path: '/',
    element: <WithHeader withLinks={false} component={<Auth />} />
  },
  {
    path: '/active-players',
    element: <WithHeader component={<ActivePlayers /> } />
  },
  {
    path: '/game',
    element: <WithHeader component={<Game /> } />
  },
]);

const RouterComponent = () => <RouterProvider router={router} />;

export default RouterComponent;