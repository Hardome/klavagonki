class Socket {
  constructor() {

  }

  setSocket (){
    if(!this.ws){
      this.ws = new WebSocket('ws://localhost:8000');
    }
    this.ws.onmessage = (message) => {
      const mes = JSON.parse(message.data);
      switch (mes.method) {
        case 'connect':
        case 'disconnect':
          //activePlayersStore.setPlayersIds(mes.activePlayers);
          console.log(mes.method, mes.activePlayers);
          break;
        /* case 'invite':
            console.log(mes)
            setActiveModal(true);
            setFromUser(mes.from);
            //const statusInvite = window.confirm('Вас пригласили в игру, играем?');
            //soket.inviteRes(statusInvite, store.userData.id, mes.from);
            break;
         case 'newGame':
            console.log(mes)
            gameStore.setGameId(mes.gameId);
            navigate(`/game/${mes.gameId}`);
            break;
         case 'moveMade':
            console.log(mes)
            gameStore.setMode(mes.gameData.gameData.nextMove);
            gameStore.setField(mes.gameData.gameData.field);
            break;
         case 'getChat':
            console.log(mes)
            gameStore.setMessages(mes.chat);
            break;
         case 'messageSent':
            console.log(mes)
            gameStore.setMessages(mes.messages);
            break;
         case 'gameEnd':
            console.log(mes)
            gameStore.setActive(mes.gameResult);
            break;*/
      }
    }
  }

  connect(userId) {
    this.ws.send(JSON.stringify({
      method: 'connect',
      userId: userId
    }));
  }

  logout(userId) {
    this.ws.send(JSON.stringify({
      method: 'disconnect',
      userId: userId
    }));
  }

  invite(id, userId) {
    this.ws.send(JSON.stringify({
      method: 'invite',
      invitedId: id,
      fromUser: userId
    }));
  }

  inviteRes(inviteStatus, id, fromUserId) {
    this.ws.send(JSON.stringify({
      method: 'inviteResult',
      invitedId: id,
      fromUser: fromUserId,
      inviteStatus
    }));
  }

  makeMove(field, gameId, role) {
    this.ws.send(JSON.stringify({
      method: 'makeMove',
      gameId,
      field,
      role
    }));
  }

  gameEnd(winner, gameId, players) {
    this.ws.send(JSON.stringify({
      method: 'gameEnd',
      winner,
      gameId,
      players
    }));
    console.log('GAMEEND');
  }

  getChat(gameId, idUser) {
    this.ws.send(JSON.stringify({
      method: 'getChat',
      gameId,
      idUser
    }));
  }

  sendMessage(idUser, messageText, gameId) {
    this.ws.send(JSON.stringify({
      method: 'sendMessage',
      idUser,
      messageText,
      gameId
    }));
  }
}

export default new Socket();