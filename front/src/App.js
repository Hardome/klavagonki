import './App.css';
import Routes from './Routes';
import {Provider} from 'mobx-react';
import userStore from './stores/UserStore';
import gameStore from "./stores/GameStore";

const stores = {
  userStore,
  gameStore
}

function App() {
  return (
    <Provider stores={stores}>
      <Routes/>
    </Provider>
  );
}

export default App;
